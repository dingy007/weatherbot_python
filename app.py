from flask import Flask, request, make_response, jsonify
import requests

app = Flask(__name__)

# **********************
# UTIL FUNCTIONS : START
# **********************

def getjson(url):
    resp =requests.get(url)
    return resp.json()

def getWeatherInfo(location):
    API_ENDPOINT = "http://api.openweathermap.org/data/2.5/weather?APPID=8a81d247d650cb16469c4ba3ceb7d265&q={}".format(location)
    data = getjson(API_ENDPOINT)
    code = data["cod"]
    if code == 200:
        return data["weather"][0]["description"]

# **********************
# UTIL FUNCTIONS : END
# **********************

# *****************************
# Intent Handlers funcs : START
# *****************************

def getWeatherIntent(req):
    location = req.get("queryResult").get("parameters").get("location")
    location = location.lower()
    info = getWeatherInfo(location)
    return f"Currently in {location} , its {info}"


# ***************************
# Intent Handlers funcs : END
# ***************************


# *****************************
# WEBHOOK MAIN ENDPOINT : START
# *****************************

@app.route('/', methods=['POST'])
def webhook():
   req = request.get_json(silent=True, force=True)
   intent_name = req["queryResult"]["intent"]["displayName"]

   # Braching starts here
   if intent_name == 'GetWeatherIntent':
       respose_text = getWeatherIntent(req)
   else:
       respose_text = "No intent matched"
   # Branching ends here

   # Finally sending this response to Dialogflow.
   return make_response(jsonify({'fulfillmentText': respose_text}))

# ***************************
# WEBHOOK MAIN ENDPOINT : END
# ***************************


if __name__ == '__main__':
   app.run(debug=True, host='0.0.0.0', port=5000)